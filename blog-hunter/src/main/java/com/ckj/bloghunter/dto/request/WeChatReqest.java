package com.ckj.bloghunter.dto.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author c.kj
 * @Description
 * @Date 2020-01-08
 * @Time 18:52
 *
 **/
@Data
public class WeChatReqest  implements Serializable {

    private static final long serialVersionUID = -3215090123894869218L;

    private BaseInsertDTO baseInsertDTO;
}
