/**
 * 打赏二维码
 */
var moneyUrl ="https://6465-dev-01d22a-1300573878.tcb.qcloud.la/money/WechatIMG1908.jpeg?sign=2ab96530056122e5a90ae49710856279&t=1572602416"

/**
 * 公众号二维码
 */
var wechatUrl ="https://6465-dev-01d22a-1300573878.tcb.qcloud.la/money/WechatIMG1910.jpeg?sign=77e07e1e3960c875a63df31df158141c&t=1572602719"

/**
 * 云开发环境
 */
var env ="dev-01d22a"
/**
 * 个人文章操作枚举
 */
var postRelatedType = {
    COLLECTION: 1,
    ZAN: 2,
    properties: {
        1: {
            desc: "收藏"
        },
        2: {
            desc: "点赞"
        }
    }
};

module.exports = {
    postRelatedType: postRelatedType,
    moneyUrl:moneyUrl,
    wechatUrl:wechatUrl,
    env:env
}