
package com.ckj.bloghunter.dao.util;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

public interface MyMapper<T> extends Mapper<T>, MySqlMapper<T> {
    // 特别注意，该接口不能被扫描到，否则会出错

    //    /**
    //     * @return
    //     */
    //    @UpdateProvider(type = UamSpecialProvider.class, method = "dynamicSQL")
    //    int updateValid(@Param("id") long id, @Param("valid") boolean valid);

}
