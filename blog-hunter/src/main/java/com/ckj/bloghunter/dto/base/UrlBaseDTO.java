package com.ckj.bloghunter.dto.base;

import java.io.Serializable;
import java.util.Date;

import com.ckj.bloghunter.domain.service.common.ShowEnum;
import com.ckj.bloghunter.domain.service.common.YesEnum;

import lombok.Data;

/**
 * @author c.kj
 * @Description
 * @Date 2019-12-19
 * @Time 18:13
 **/
@Data
public class UrlBaseDTO implements Serializable {

    private static final long serialVersionUID = -3215090123894869218L;

    private String            link;

    private YesEnum           isSave;

    private Date              createDate;

    private String            imgUrl;

    private ShowEnum          isShow;

    private Integer           totalVisit;

}
