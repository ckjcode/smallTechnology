package com.ckj.bloghunter.dao.dataobject;

import javax.persistence.Column;
import javax.persistence.Table;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Table(name = "small_technology_blog")
@Builder
@Data
@Getter
@Setter
public class SmallTechnologyBlog {
    @Column(name = "_id")
    private String  id;

    @Column(name = "totalComments")
    private String  totalComments;

    @Column(name = "isShow")
    private Integer isShow;

    private String  label;

    @Column(name = "originalUrl")
    private String  originalUrl;

    @Column(name = "createTime")
    private String  createTime;

    @Column(name = "totalVisits")
    private Integer totalVisits;

    @Column(name = "totalZans")
    private Integer totalZans;

    private String  title;

    @Column(name = "sourceFrom")
    private String  sourceFrom;

    private Integer classify;

    private String  digest;

    private String  author;

    private String  timestamp;

    @Column(name = "contentType")
    private String  contentType;

    @Column(name = "defaultImageUrl")
    private String  defaultImageUrl;

    @Column(name = "totalCollection")
    private String  totalCollection;

    @Column(name = "uniqueId")
    private String  uniqueId;

    @Column(name = "extraInfo")
    private String  extraInfo;

    private String  content;

}
