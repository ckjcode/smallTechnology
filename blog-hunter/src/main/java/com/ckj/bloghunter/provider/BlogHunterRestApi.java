package com.ckj.bloghunter.provider;

import com.ckj.bloghunter.dto.response.WeChatResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ckj.bloghunter.domain.service.BlogComponentService;
import com.ckj.bloghunter.dto.request.PreviewHunterBlogRequest;
import com.ckj.bloghunter.dto.response.PreviewHunterBlogResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @author c.kj
 * @Description
 * @Date 2019-12-18
 * @Time 10:27
 **/
@RequestMapping("/blogHunter")
@RestController
@Slf4j
@Api(tags = "blogHunter", description = "blogHunter")
public class BlogHunterRestApi {

    @Autowired
    private BlogComponentService blogComponentService;

    @ApiOperation(value = "preview hunter blog", notes = "preview hunter blog")
    @RequestMapping(value = { "/previewHunterBlog" }, method = RequestMethod.POST)
    public PreviewHunterBlogResponse previewHunterBlog(@ApiParam(name = "preview hunter blog") @RequestBody PreviewHunterBlogRequest req) {
        return blogComponentService.previewHunterBlog(req);
    }


    @ApiOperation(value = "import cloud development db", notes = "import cloud development db")
    @RequestMapping(value = { "/import cloud development db" }, method = RequestMethod.POST)
    public WeChatResponse insertData(@ApiParam(name = "insert data") @RequestBody PreviewHunterBlogRequest req) {
        return blogComponentService.insertData(req);
    }

}
