package com.ckj.bloghunter.domain.service;

import com.ckj.bloghunter.dao.dataobject.SmallTechnologyBlog;
import com.ckj.bloghunter.dto.request.PreviewHunterBlogRequest;
import com.ckj.bloghunter.dto.response.PreviewHunterBlogResponse;
import com.ckj.bloghunter.dto.response.WeChatResponse;

/**
 * @author c.kj
 * @Description
 * @Date 2019-12-18
 * @Time 19:23
 **/
public interface BlogComponentService {

    PreviewHunterBlogResponse previewHunterBlog(PreviewHunterBlogRequest req);

    WeChatResponse insertData(PreviewHunterBlogRequest req);
}
