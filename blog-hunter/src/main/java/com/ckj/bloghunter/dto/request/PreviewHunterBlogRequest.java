package com.ckj.bloghunter.dto.request;

import java.io.Serializable;
import java.util.List;

import com.ckj.bloghunter.dto.base.UrlBaseDTO;

import lombok.Data;

/**
 * @author c.kj
 * @Description
 * @Date 2019-12-19
 * @Time 17:56
 **/
@Data
public class PreviewHunterBlogRequest implements Serializable {

    private static final long serialVersionUID = -3215090123894869218L;

    private List<UrlBaseDTO>  urls;

}
