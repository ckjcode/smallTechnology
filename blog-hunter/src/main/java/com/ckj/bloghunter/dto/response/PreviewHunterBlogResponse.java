package com.ckj.bloghunter.dto.response;

import java.io.Serializable;
import java.util.List;

import com.ckj.bloghunter.dao.dataobject.SmallTechnologyBlog;

import lombok.Data;

/**
 * @author c.kj
 * @Description
 * @Date 2019-12-19
 * @Time 17:58
 **/
@Data
public class PreviewHunterBlogResponse implements Serializable {

    private static final long         serialVersionUID = -3215090123894861218L;

    private List<SmallTechnologyBlog> blogs;

    private Boolean                   success;
}
